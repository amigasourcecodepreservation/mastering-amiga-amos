= Mastering Amiga Amos

This is the digital version of the book `Mastering Amiga Amos`.

*Author:* Phil South.

*Original Publisher:* Bruce Smith Books

*Year:* 1993

Released under **Creative Commons:** 2018

== Description

_AMOS has revolutionised all forms of programming on the Amiga. What is more it has made it possible for every Amiga owner to create stunning sound and grapichs with the absoulute minimum of fuss. It's great for games, education and even for presenting serious programs and utilities._

This enlarged and revised edition has more programs than ever before and includes tutorials on:

* Windows, Text and Menus
* Screens, Sprites and Bobs
* Icons and Screen Block
* Music and Sound
* Object Movement
* Sprite X, CText and TOME
* AMOS Compiler and 3D

== Read this book: 

* https://gitlab.com/amigasourcecodepreservation/mastering-amiga-amos/blob/master/pdf/mastering-amiga-amos-1993-south.pdf?expanded=true&viewer=rich[Scanned pdf of the original print]

== How To Generate the Book

In the future we would like to convert the book to the better-than-markdown https://asciidoctor.org/[AsciiDoc] format, so that it could be converted to many formats, while still being readable on a classic computer. 

== Contributing

If you'd like to help out, here is a suggested to-do:

* Convert the original scan to AsciiDoc.
* Add bookmarks and metadata to the scanned pdf.
* Add the code examples
* Any other interesting items regarding the book? Commercial, Reviews, Articles? An optimized pdf?

== License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

Many thanks to Phil South 

== Copyright & Author

Phil South, © 1993 - 2018

Thanks to asymetrix at Ultimate Amiga for the scan


